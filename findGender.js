// 1. Find all people who are Agender

const userDetails = require('./inventory');

const gender = userDetails.filter(user => user['gender'] === "Agender");
console.log(gender);

/*[
  {
    id: 5,
    first_name: 'Jocelyne',
    last_name: 'Casse',
    email: 'jcasse4@ehow.com',
    gender: 'Agender',
    ip_address: '176.202.254.113'
  },
  {
    id: 8,
    first_name: 'Anna-diane',
    last_name: 'Wingar',
    email: 'awingar7@auda.org.au',
    gender: 'Agender',
    ip_address: '148.229.65.98'
  }
]
*/