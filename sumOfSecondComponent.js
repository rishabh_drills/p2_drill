// 3. Find the sum of all the second components of the ip addresses.

const userDetails = require('./inventory');

function sumOfSecondComponent(userDetails){
     const secondAdd = userDetails.map((element) => {  
      return element['ip_address'].split('.').map((splitValue) => parseFloat(splitValue))
      
    }).reduce((acc, curr) => {
        return acc += curr[1];
    }, 0)
    
        return secondAdd;
}
const result = sumOfSecondComponent(userDetails);
console.log(result);