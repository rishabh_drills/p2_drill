// 4. Find the sum of all the fourth components of the ip addresses.

const userDetails = require('./inventory');

function sumOfFourthComponent(userDetails){
     const fourthAdd = userDetails.map((element) => {  
      return element['ip_address'].split('.').map((splitValue) => parseFloat(splitValue))
      
    }).reduce((acc, curr) => {
        return acc += curr[3];
    }, 0)
    
        return fourthAdd;
}
const result = sumOfFourthComponent(userDetails);
console.log(result);